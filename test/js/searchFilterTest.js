/**
 * Created by Pandithurai Adiyan on 7/6/16.
 */
/* Testing Search Filter Code */

describe('Search filter', function () {

    var $filter;

    beforeEach(function () {
        module('app');

        inject(function (_$filter_) {
            $filter = _$filter_;
        });
    });

    it('User Andrew should get filtered', function () {
        // Arrange.
        var foo = 'Andrew', result;

        // Act.
        result = $filter('searchUser')(foo, 'searchString');

        // Assert.
        expect(result.length).toEqual(1);
    });
});
