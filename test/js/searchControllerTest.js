/**
 * Created by Pandithurai Adiyan on 7/6/16.
 */
/* Testing Search Controller Code */
describe('Search Controller', function () {

    beforeEach(module('app'));
    var $controller;
    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));

    describe('Users List', function () {
        it('Users List should not be empty', function () {
            var $scope = {};
            var controller = $controller('searchController', { $scope: $scope });
            expect($scope.users.length).toBeGreaterThan(0);
        });
    });
});