## Overview

A simple search app using Angular JS framework.

## Requirements

* nodejs
* npm
* gulp

## Getting Started

1. run `npm start`
2. Open http://localhost:8080/ in your browser.
3. A page showing all users and an option to search users will get opened.
4. Type search string in the input field and verify the result.

## Testing

1. run `npm test`
2. Open http://localhost:8081/ in your browser.
3. verify the test results.