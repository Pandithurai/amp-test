/**
 * Created by Pandithurai Adiyan on 7/6/16.
 */
var express = require('express');
var app = express();

app.use('/', express.static('dist'));
// viewed at http://localhost:8080
app.listen(8080);
console.log('server started at http://localhost:8080');