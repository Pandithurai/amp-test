/**
 * Created by Pandithurai Adiyan on 7/5/16.
 */
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    runSequence = require('run-sequence');

/*This task starts first and run others task in sequence*/
gulp.task('default', function(callback) {
    runSequence('clean','concat','copy-to-test','foundation-style','sass','copy','copy-index',callback);
});

/*Removes the dist folder*/
gulp.task('clean', function () {
    return gulp.src(['dist/'], {read: false})
        .pipe(clean());
});

/*Combines all filter, controller and app js files to a single javascript file */
gulp.task('concat', function() {
    return gulp.src('src/**/*.js')
        .pipe(concat('search.js'))
        .pipe(gulp.dest('dist/js'));
});

/*Copy search.js file to test folder for jasmine testing */
gulp.task('copy-to-test', function (callback) {
    gulp.src(['dist/js/search.js'])
        .pipe(gulp.dest('test/js'));
    callback();
});

/*Compile and copy foundation SCSS file to dist/lib folder  */
gulp.task('foundation-style', function () {
    return gulp.src('node_modules/foundation/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('dist/lib/css'));
});

/*Compile and copy app specific SCSS file to dist folder  */
gulp.task('sass', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('dist/css'));
});

/*Copy angular js library to dist/lib folder*/
gulp.task('copy', function (callback) {
    gulp.src(['node_modules/angular/angular.js'])
        .pipe(gulp.dest('dist/lib/js'));
    callback();
});

/*Copy index.html to dist folder to load via express server*/
gulp.task('copy-index', function (callback) {
    gulp.src(['src/index.html'])
        .pipe(gulp.dest('dist/'));
    callback();
});




