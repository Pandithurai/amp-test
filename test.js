/**
 * Created by Pandithurai Adiyan on 7/6/16.
 */
var express = require('express');
var app = express();

app.use('/', express.static('test'));
// viewed at http://localhost:8081
app.listen(8081);
console.log('server started at http://localhost:8081');