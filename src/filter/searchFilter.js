/**
 * Created by Pandithurai Adiyan on 7/5/16.
 * @input: Users List and search String
 * @Output: filtered list
 */
app.filter('searchUser', function(){

    return function(users, searchString){

        if(!searchString){
            return users;
        }

        var result = [];

        searchString = searchString.toLowerCase();

        // Loop through the users list and check for the match
        users.forEach(function(item){
            if(item.name.toLowerCase().indexOf(searchString) !== -1){
                result.push(item);
            }

        });

        return result;
    };

});
