/**
 * Created by Pandithurai Adiyan on 7/5/16.
 * Populate users list with user details
 * In realtime this data can be fetched from Mongodb
 */

app.controller("searchController", function($scope) {

    $scope.users = [
        {
            name: 'Andrew',
            url: 'http://lorempixel.com/200/100/people/1/'
        },
        {
            name: 'Peter',
            url: 'http://lorempixel.com/200/100/people/2/'
        },
        {
            name: 'Claudia',
            url: 'http://lorempixel.com/200/100/people/3/'
        },
        {
            name: 'Delvis',
            url: 'http://lorempixel.com/200/100/people/4/'
        },
        {
            name: 'Elizabeth',
            url: 'http://lorempixel.com/200/100/people/5/'
        },
        {
            name: 'Florida',
            url: 'http://lorempixel.com/200/100/people/6/'
        },
        {
            name: 'Grahame',
            url: 'http://lorempixel.com/200/100/people/7/'
        },
        {
            name: 'Mark',
            url: 'http://lorempixel.com/200/100/people/8/'
        },
        {
            name: 'Scott',
            url: 'http://lorempixel.com/200/100/people/9/'
        },
        {
            name: 'Kumar',
            url: 'http://lorempixel.com/200/100/people/10/'
        }
    ];
});